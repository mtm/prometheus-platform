Changelog
=========

1.0.0
-----

Initial version:

- with Centos support
- manage all artifacts available on https://prometheus.io/download/
